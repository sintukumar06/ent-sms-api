/* Collections & Indexes */
// 1. Create collection 'schedules' with index contactPoint
db.createCollection("schedules")

/* Insert 'schedules' */
db.schedules.insert({
    "dayOfTheWeek" : "Monday",
    "startTime" : {
        "hour" : "08",
        "minutes" : "00",
        "seconds" : "00",
        "millis" : "000"
    },
    "endTime" : {
        "hour" : "17",
        "minutes" : "15",
        "seconds" : "00",
        "millis" : "000"
    }
})
db.schedules.insert({
    "dayOfTheWeek" : "Tuesday",
    "startTime" : {
        "hour" : "08",
        "minutes" : "00",
        "seconds" : "00",
        "millis" : "000"
    },
    "endTime" : {
        "hour" : "17",
        "minutes" : "15",
        "seconds" : "00",
        "millis" : "000"
    }
})
db.schedules.insert({
    "dayOfTheWeek" : "Wednesday",
    "startTime" : {
        "hour" : "08",
        "minutes" : "00",
        "seconds" : "00",
        "millis" : "000"
    },
    "endTime" : {
        "hour" : "17",
        "minutes" : "15",
        "seconds" : "00",
        "millis" : "000"
    }
})
db.schedules.insert({
    "dayOfTheWeek" : "Thursday",
    "startTime" : {
        "hour" : "08",
        "minutes" : "00",
        "seconds" : "00",
        "millis" : "000"
    },
    "endTime" : {
        "hour" : "17",
        "minutes" : "15",
        "seconds" : "00",
        "millis" : "000"
    }
})
db.schedules.insert({
    "dayOfTheWeek" : "Friday",
    "startTime" : {
        "hour" : "08",
        "minutes" : "00",
        "seconds" : "00",
        "millis" : "000"
    },
    "endTime" : {
        "hour" : "17",
        "minutes" : "15",
        "seconds" : "00",
        "millis" : "000"
    }
})

/* Insert 'settings' */
db.settings.insert({
    "messagingServiceName" : "Insurance",
    "apiKey" : "FkjIgegMx4aYbcbGv98L",
    "tenantId" : "L4",
    "messagingServiceId" : "MG2e63b86bc2ad9f8fd52903f436fe5b88",
    "serviceProvider" : "twilio",
    "preferenceType" : "Insurance",
    "clientCallbackEndpoint" : {
        "inbound" : [ 
            {
                "applicationName" : "PolicyCenter",
                "protocol" : "HTTP",
                "type" : "REST",
                "host" : "sae1dpcl41p1",
                "port" : "8081",
                "path" : "/pc/service/entsmsapi/notifyOptIn/",
                "notify" : true,
                "tenantId" : "L4",
                "credentials" : {
                    "username" : "svc_Int_GlobalAcct",
                    "password" : "$Uq_fi2raz5s"
                }
            }
        ],
        "outbound" : []
    }
})
/* Insert 'templates' */
db.templates.insert({
    "templateId" : "sms-ui-welcome-template",
    "keywordsTemplateMapping" : {
        "Y" : "no-response-welcome-template",
        "YES" : "no-response-welcome-template",
        "START" : "no-response-welcome-template",
        "UNSTOP" : "no-response-welcome-template"
    },
    "message" : "State Auto: Our CSR signed you up to receive texts. To confirm, reply YES. Visit stateauto.com/text for T&Cs. Reply STOP to end texting. Msg&data rates apply.",
    "tenantId" : "L4",
    "version" : "1.0"
})
db.templates.insert({
    "templateId" : "sms-ui-outside-business-hours-template",
    "keywordsTemplateMapping" : {
        "STOP" : "no-response-welcome-template",
        "STOPALL" : "no-response-welcome-template",
        "UNSUBSCRIBE" : "no-response-welcome-template",
        "CANCEL" : "no-response-welcome-template",
        "END" : "no-response-welcome-template",
        "STOPALL" : "no-response-welcome-template"
    },
    "message" : "Hello, thank you for reaching out. I received your message and you can expect to hear from me by the next business day. If that feels like too long to wait to speak with someone, please call customer care at 877-SA-CLAIM",
    "tenantId" : "L4",
    "version" : "1.0"
})