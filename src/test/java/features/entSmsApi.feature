Feature: MVR SYSTEM API Test Script

  Background:
    * url 'https://mule-l1.apps.stateauto.com:9013/sms/1.2/api'
    * configure ssl = true

Scenario: get Healthcheck validation
    Given url 'https://mule-l1.apps.stateauto.com:9013/sms/1.2'
    When method get
    Then status 200

    
Scenario: Update Contactpoint Preference
    Given path '/preferences/contactpoint'
    Given header Content-Type = 'application/json'
      And header applicationName = 'Flyreel'
      And header tenantId = 'L1-1'
      And header Authorization = 'Basic cGM6bDFwY3Bhc3M='
      And request read('classpath:karate/updateContactPointWithCallbackUrl.json')
     When method PUT
     Then status 202
      And match response.responseId contains "Used_for_tracking"
      And match response.responseStatus contains "success"